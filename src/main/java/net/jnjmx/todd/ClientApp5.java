package net.jnjmx.todd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.Scanner;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.ObjectInstance;
import javax.management.ObjectName;

import javax.management.remote.*;


public class ClientApp5 {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		System.out.println("Todd ClientApp5... Accessing JMX Beans (and the 'Uptime' property of TODD MBean 'Server')");

		try {
			String server = "192.168.56.11";

			if (args.length >= 1) {
				server = args[0];
			}

            System.out.println("Connecting to  TODD at "+server+" ...");
            System.out.println("Creating several sessions for a few seconds");
            
            Client c = new Client(server);
			Client c1 = new Client(server);
			Client c2 = new Client(server);
			Client c3 = new Client(server);
			Client c4 = new Client(server);
			Client c5 = new Client(server);
			Client c6 = new Client(server);
            
            Thread.sleep(6000);
			
			c.close();
			c1.close();
			c2.close();
			c3.close();
			c4.close();
			c5.close();
			c6.close();

			System.out.println("Created Sessions Disconnected");
					
			System.exit(0);
		} catch (Exception ex) {
			System.out.println("Error: unable to connect to MBean Server");
			ex.printStackTrace();
			System.out.println(ex.getMessage());
		}
	}
}
