#!/bin/bash
apt install -y python openssh-server
mkdir /root/.ssh
echo 'PasswordAuthentication no' >> /etc/ssh/sshd_config
echo 'PermitRootLogin without-password' >> /etc/ssh/sshd_config
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCx+NKNnbZyrsHzdzKFT7yE1wPr0RYxKmh3d5oNZpDtwKFsksyt9gXohIXMXEOJuVacsT3kVQc3HZA61WAyYlVWN33t5QXgtQQvRmawdeHk7ZRh//RoAMgOVwH4Etmyq2+fCh/1U2mFyx6L7/pIZc9qr+q6OioabPgu+/tzl6O0LlBkZVqqS9zFXxPpkBQiwCqnZxOVJwMZ1c3JXrpmCEfUJdGCYIXqc1w7CWgryEZVBbFhkOYNBxZCvJgIERDwZC5b66Lu4pJmXm2TaR9tHfWZvoCO6mbsjK8Z2XmiRtVcrp8F03gL4aPeLBDcFhrVSdHSJUF6emyRu6omnnDALP1iF6PNgP2Adpb5ERJ7G4WH0OiTBGjIN+Bqf39x2XDicyTY128/Wiy8j1mcwfPcXK94Z4O8R+VwVo5ssYhRjb9K+seFdakwi1DeeRS6JMnJce0hbUFA5AOuJx4J8fPwa+siquSyqjLGJnJfJixYUqjifFAkzW2QKifh7XRttYPYqUk= root@AnsiableA' > /root/.ssh/authorized_keys
service ssh restart
sudo iptables -I INPUT -p tcp -m tcp --dport 5666 -j ACCEPT

